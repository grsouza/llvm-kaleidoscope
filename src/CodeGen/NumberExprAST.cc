#include "AST/NumberExprAST.hh"

llvm::Value *NumberExprAST::codegen(IREmitter *emitter) {
  return llvm::ConstantFP::get(emitter->llvm_context(), llvm::APFloat(Val));
}