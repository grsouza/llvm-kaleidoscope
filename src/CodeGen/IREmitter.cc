#include "CodeGen/IREmitter.hh"

IREmitter::IREmitter(llvm::Module *module)
    : module(std::unique_ptr<llvm::Module>(module)),
      builder(std::unique_ptr<llvm::IRBuilder<>>(
          new llvm::IRBuilder<>(module->getContext()))) {}

IREmitter::~IREmitter() {
  builder.reset();
  module.reset();
}

llvm::LLVMContext &IREmitter::llvm_context() { return module->getContext(); }

llvm::Value *IREmitter::getNamedValue(const std::string &name) {
  return namedValues[name];
}

void IREmitter::setNamedValue(const std::string &name, llvm::Value *value) {
  namedValues[name] = value;
}