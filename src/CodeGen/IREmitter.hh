#pragma once

#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Value.h>

#include <map>
#include <memory>
#include <string>

class IREmitter {
  std::map<std::string, llvm::Value *> namedValues;
  std::unique_ptr<llvm::Module> module;
  std::unique_ptr<llvm::IRBuilder<>> builder;

public:
  IREmitter(llvm::Module *module);
  ~IREmitter();

  llvm::LLVMContext &llvm_context();

  llvm::Value *getNamedValue(const std::string &name);
  void setNamedValue(const std::string &name, llvm::Value *value);
};