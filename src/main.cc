#include "CodeGen/IREmitter.hh"
#include "Parser/Parser.hh"
#include <iostream>

int main() {
  llvm::LLVMContext TheContext;
  IREmitter *emitter = new IREmitter(new llvm::Module("cool lang", TheContext));

  Parser *parser = new Parser();

  int tok = tok_eof;
  while ((tok = parser->GetNextToken()) != tok_eof)
    std::cout << tok << std::endl;

  return 0;
}
