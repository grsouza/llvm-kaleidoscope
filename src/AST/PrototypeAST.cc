#include "AST/PrototypeAST.hh"

PrototypeAST::PrototypeAST(const std::string &name,
                           std::vector<std::string> Args)
    : Name(name), Args(std::move(Args)) {}

const std::string &PrototypeAST::getName() const { return Name; }