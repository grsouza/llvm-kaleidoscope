#pragma once

#include "CodeGen/IREmitter.hh"
#include "llvm/IR/Value.h"
#include <string>
#include <vector>

class PrototypeAST {
  std::string Name;
  std::vector<std::string> Args;

public:
  PrototypeAST(const std::string &name, std::vector<std::string> Args);
  const std::string &getName() const;

  virtual llvm::Value *codegen(IREmitter *emitter);
};
