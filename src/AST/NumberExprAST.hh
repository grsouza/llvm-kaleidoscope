#pragma once

#include "AST/ExprAST.hh"

/// NumberExprAST - Expression class for numeric literals like "1.0".
class NumberExprAST : public ExprAST {
  double Val;

public:
  NumberExprAST(double Val);
  virtual llvm::Value *codegen(IREmitter *emitter);
};
