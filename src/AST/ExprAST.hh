#pragma once

#include "CodeGen/IREmitter.hh"
#include <llvm/IR/Value.h>

/// ExprAST - Base class for all expression nodes.
class ExprAST {
public:
  virtual ~ExprAST() {}
  virtual llvm::Value *codegen(IREmitter *emitter) = 0;
};
