#pragma once

#include "AST/ExprAST.hh"
#include "AST/PrototypeAST.hh"
#include <memory>

class FunctionAST {
  std::unique_ptr<PrototypeAST> Proto;
  std::unique_ptr<ExprAST> Body;

public:
  FunctionAST(std::unique_ptr<PrototypeAST> Proto,
              std::unique_ptr<ExprAST> Body);
  virtual llvm::Value *codegen(IREmitter *emitter);
};
