#pragma once

#include "AST/ExprAST.hh"
#include <memory>
#include <string>
#include <vector>

/// CallExprAST - Expression class for function calls.
class CallExprAST : public ExprAST {
  std::string Callee;
  std::vector<std::unique_ptr<ExprAST>> Args;

public:
  CallExprAST(const std::string &Callee,
              std::vector<std::unique_ptr<ExprAST>> Args);
  virtual llvm::Value *codegen(IREmitter *emitter);
};
