#pragma once

#include "AST/ExprAST.hh"
#include <memory>

/// BinaryExprAST - Expression class for a binary operator.
class BinaryExprAST : public ExprAST {
  char Op;
  std::unique_ptr<ExprAST> LHS, RHS;

public:
  BinaryExprAST(char op, std::unique_ptr<ExprAST> LHS,
                std::unique_ptr<ExprAST> RHS);
  virtual llvm::Value *codegen(IREmitter *emitter);
};
