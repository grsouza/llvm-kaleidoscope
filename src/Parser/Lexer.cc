#include "Parser/Lexer.hh"
#include <string>

Lexer::Lexer() {}

int Lexer::Lex() {
  LastChar = ' ';

  // skip any whitespace
  while (isspace(LastChar))
    LastChar = getchar();

  // identifier: [a-zA-Z][a-zA-Z0-9]*
  if (isalpha(LastChar)) {
    IdentifierStr = LastChar;
    while (isalnum((LastChar = getchar())))
      IdentifierStr += LastChar;

    if (IdentifierStr == "def")
      return tok_def;
    if (IdentifierStr == "extern")
      return tok_extern;
    return tok_identifier;
  }

  // Number: [0-9.]*
  if (isdigit(LastChar) || LastChar == '.') {
    std::string NumStr;
    do {
      NumStr += LastChar;
      LastChar = getchar();
    } while (isdigit(LastChar) || LastChar == '.');

    NumVal = strtod(NumStr.c_str(), 0);
    return tok_number;
  }

  if (LastChar == '#') {
    do
      LastChar = getchar();
    while (LastChar != EOF && LastChar != '\n' && LastChar != '\r');

    if (LastChar != EOF)
      return Lex();
  }

  return tok_eof;
}

const std::string &Lexer::getIdentifier() const { return IdentifierStr; }

double Lexer::getNumVal() { return NumVal; }