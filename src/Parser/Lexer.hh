#pragma once

#include <string>

// The lexer returns tokens [0-255] if it is an unknown character, otherwisse
// one of these for known thins.
enum Token {
  tok_eof = -1,

  // commands
  tok_def = -2,
  tok_extern = -3,

  // primary
  tok_identifier = -4,
  tok_number = -5,
};

class Lexer {
  std::string IdentifierStr;
  double NumVal;
  char LastChar;

public:
  Lexer();
  int Lex();

  const std::string &getIdentifier() const;
  double getNumVal();
};
