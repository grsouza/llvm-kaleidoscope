#pragma once

#include "AST/ExprAST.hh"
#include "Parser/Lexer.hh"

class Parser {
  int CurTok;
  Lexer *lexer;

public:
  Parser();
  int GetNextToken();

private:
  /// numberexpr ::= number
  std::unique_ptr<ExprAST> parseNumberExpr();
};
